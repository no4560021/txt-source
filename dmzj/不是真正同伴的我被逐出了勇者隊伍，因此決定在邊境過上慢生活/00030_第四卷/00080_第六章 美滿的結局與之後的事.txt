在古代精靈遺跡的激戰結束，我們返回佐爾坦的第二天早上。
「呼啊——」
我打了個哈欠露出毫無幹勁的表情在打掃店外的衛生。
「亞雷斯那傢伙，竟然把店裡搞成這樣」
從遺跡回來的我和莉特所看到的，是被掀了個底朝天的我的愛店。
雖然對此我很憤慨，但莉特更是大發雷霆。

可是亞雷斯這個罪魁禍首已經被我砍了。
莉特將怒火轉變為活力，明明剛剛從激戰中回來卻在佐爾坦裡東奔西走張羅著店鋪的修繕事宜。
「費用我來出給我用最快的速度把店修好」
面對莉特那不由分說地表情，我只能默默點頭。
雖然安排是安排好了但修理今天才開始。所以今天依舊關店歇業。
工匠們馬上就會來，因此我就在外面打掃打掃衛生。
「啊，下雪了」
潔白的雪花映入了眼簾。
稀疏的雪花在風中不斷飄蕩遲遲不肯落下。
雖然大概積不起來，不過我覺得南方的雪這樣也別有一番風情。
「哥哥」
身後傳來了呼喚。

扭過頭去，身著雪白的連衣裙，頭戴與衣服十分相稱的白色帽子的露媞正站在那裡。
這件連衣裙是和我一起在平民區裡買的。
它並不是什麼魔法房間。
而且露媞的腰上也沒有佩劍。戒指，護身符之類的魔法裝備現在也全都摘了下來。

在我面前的就只是一個隨處可見的普通少女。
不過。
「阿嚏」
「你穿這麼薄肯定會被凍著吧」
即便在佐爾坦，在下雪天裡只穿了一件連衣裙連外套都沒有肯定會冷。
「誒嘿嘿」
露媞雖然打了個噴嚏，但她卻很開心的樣子。
「很久沒有覺得冷過了，好開心」
「當心感冒」
「我還沒有得過感冒呢。真期待第一次得感冒」
我脫下上衣，披在了露媞身上。
「給，不過很抱歉只有男裝」
「......好暖和」
她像是名普通的女孩一樣如此低語，嫣然一笑。
露媞並沒有失去『勇者』的加護。『勇者』至今仍留在她的體內。

但是，露媞體內新產生的那個無名加護，現在名字變成了『sin』。據露媞所說，那個準確來講似乎並不是加護。

她在接觸『sin』的時候，好像並沒有像接觸加護時那樣感受到迪米斯神的存在。
也就是說這並不是由神所賦予的“加護”。要是被聖方教會知道了那可不得了。

『sin』也不存在加護衝動。
也就是說，『sin』並沒有它的職責。
除此之外，『sin』在其他方面基本和加護一樣。它有等級也有技能。想讓它成長只需要去打倒擁有加護的對手就夠了。
『sin』的技能很特殊，似乎並不是只要提升等級就可以自由選擇，而是需要滿足條件才能獲得。

露媞獲得的技能是“支配者”。它能夠將觸碰到的對象的加護技能無效化或者令其強制發動，效果十分驚人。

這個技能在這個大半的加護能力都是戰鬥向的世界可以說是犯規級別的存在。
恐怕就連傳說中的惡魔爵們也已不是露媞的對手。

但是對露媞來說最大的福音，就在於這個能力也可以對自己發動。
現在露媞支配了自己的『勇者』加護，將完全耐性和衝動已經基本全都無效化了。
「阿嚏」
「雷德，露媞，這麼冷的天你們打算在外面待到什麼時候啊？」
莉特擔心一直不回去的我們而來到了店外。
「這些飄雪真漂亮呢。但是會感冒的呀」
「是啊。露媞，你是想吃了早飯再去的吧？我馬上做，你到屋裡等著吧」
「嗯，好期待哥哥做的飯」
我們回到了家裡。
在穿過玄關之前，露媞和莉特再一次回首，依依不捨地眺望著在空中飄落的潔白雪花。


*  *  *


戰鬥結束之後，提奧朵拉跟阿爾貝爾立馬便和我們分別了。
他們似乎打算回到前線去。
「就此別過了」
提奧朵拉留下了一句告別的話之後，便再也沒有回頭。
露媞想要把在旅途中得到的裝備交給她，但她以「這些裝備或許總有一天在你以自身的意志選擇戰鬥的時候會需要它」而回絕了。

她應該不會再來佐爾坦了吧。
達南需要在佐爾坦的療養所裡花上半年時間專心療養。他似乎相當亂來，再怎麼說這下達南應該也不得不老實一陣子了。

戈德溫我們決定給他自由。露媞已經不再需要服用惡魔加護。
而且這次的戰鬥能獲得勝利，他的奮戰也是不可或缺的。露媞想要報答他的奮戰，因此決定對他犯過的罪行佯作不知。

這個選擇想必是勇者時期的露媞所做不出來的。
從露媞那裡拿到了足夠盤纏的戈德溫，似乎打算將聖方教會勢力較弱的南方群島諸王國定位目的地。
「在惡魔加護的事件中和聖方教會作對的我，應該也能在群島諸王國生活下去吧。對了，我也在那裡開家藥店好了」
戈德溫這麼說完，看著我笑了笑。


*  *  *


「今天的早餐是培根和番茄湯燉白豆，用剩下的番茄做的披薩，以及用檸檬汁榨的新鮮果汁」
「「我開動了！」」
清晨的餐桌現在變得比之前還要熱鬧。
「哥哥做的早餐總是那麼好吃」
露媞在笑嘻嘻地吃著早餐。
就仿彿至今一直被勇者的加護所壓抑的喜悅解放了出來一樣，露媞現在十分開心。
「之後要和提瑟一起去？」
「嗯」
露媞和提瑟打算去北區買下一座的農場，在那裡栽培藥草。
雖然藥草是有市場需求，可需要的種類很多而單位面積的收獲量卻很少。
一般的農戶因為很沒效率所以都不會去種，但露媞卻硬是想要挑戰一下栽培藥草。
「我們打算去找找看有沒有人肯賣給我們農場，我會加油的」
沒想到竟然能迎來看到露媞露出這種表情的一天。
人生被迫走上勇者之路的露媞，現在第一次打算挑戰自己想做的事。

作為哥哥，我打從心底為露媞現在的變化感到開心。


*  *  *


「喲雷德！」
岡茲來到了店裡。
「我知道你現在在休息，但是坦塔感冒了」
「要感冒藥是吧。你等我一會兒」
我從集中存放在櫃台的逃過一劫的藥中拿出一周份的感冒藥，裝在了袋子裡。
「你妹妹那邊還順利嗎？」
「嗯，已經沒事了」
我這麼說著將袋子交給岡茲之後，他露出了安心的笑容。
「那可真是太好了。到時候也要好好給我們介紹介紹啊」
我沒有對他說明情況害他擔心了。

身為佐爾坦人，他恐怕也不會過問我們的過去。他能夠分清什麼事是不能隨便觸及的。
但即便如此，我也想好好把妹妹介紹給他。哪怕得隱瞞她是勇者的事情，我也想讓平民區的大伙們知道我有一個叫做露媞的妹妹。
「當然，下次大家一起出去玩吧」
不止是露媞。我也想把提瑟和達南介紹給大家。
因為我們是一同出生入死的戰友。


*  *  *


午後。
我和露媞以及提瑟，達南來到了位於佐爾坦中央區的教會。
「至高神迪米斯啊。現在，您忠實的孩子將要在您的身邊展開最初也是最後的巡禮。他走過的人生將銘刻在『加護』之上，其罪孽也將與『加護』一同回歸到您的身邊。倘若他的『加護』功德圓滿，還懇請您引導他邁入涅槃界尼爾瓦納。倘若他尚未擁有踏入尼爾瓦納之門的資格，在他重獲新的『加護』之前，還請讓您忠實的孩子，亞雷斯的靈魂安息吧」
言畢司祭將香油滴在了沉眠與棺木中的亞雷斯的臉上。在阿瓦隆大陸萬國共通的雪玫瑰的花香微微飄散在室內。

雖然味道很香，但因為會在葬禮上用到所以雪玫瑰的香油無論如何都會讓人聯想到死人。而且由於教會在日常中也會用到，因此在每個教會都有雪玫瑰的花壇，這大概也是造成它的花香的印象固定化的原因之一吧。

說起來，阿瓦隆大陸的詩人也說過在人生中一定要創作出一首關于雪玫瑰花的詩。雖然我並沒有吟詩的才能，但心中依舊也有所感悟。

參加的人只有我們4個，外加上神父和他的助手2人而已。
作為英雄的葬禮來說這趟送行可能實在是太過寂寥了。
但是，躺在棺木裡的亞雷斯並不會抱怨什麼，只是靜靜地閉著眼睛。

根據聖方教會教義的說法，人生前所犯下的罪行會被記錄在『加護』之中。當『加護』回歸迪米斯神身邊之後，罪行就會被赦免，然後在重獲新『加護』的來世又能作為無垢的靈魂進行轉生。

但是如果沒有遵從迪米斯神的教誨，也就是不按照聖方教會的教義而活的話，迪米斯神便不會接受他的『加護』，背負著罪孽的靈魂，會在“惡魔上帝”所在的七層地獄界......賽文赫爾斯作為奴隷永受折磨。

司祭搖響了手中的鈴鐺。
「那麼，雷德先生」
「是」
我按照規矩，把一根木柴放在了亞雷斯的棺中。露媞，提瑟和達南也同樣分別放了一根木柴。
接著神父在最後又獻上了一次祈禱。這是為了向迪米斯神傳達亞雷斯是一位多麼忠實的信徒。
「這下葬禮就平安結束了。大家應該都知道，我們將在死後的7天為他進行火葬，如果各位希望的話可以在那一天再與故人做最後的相見」
「......不，還是算了」
我稍微猶豫了會兒還是拒絕了。
亞雷斯好不容易才從戰鬥中解放了出來。我想讓他安心長眠。
「我明白了」
司祭微笑著搖了一下鈴鐺。這樣一來，『賢者』亞雷斯的葬禮便悄然落下了帷幕。

加護只會寄宿在所有擁有生命的生物上。

所以，這下亞雷斯就再也不會被『賢者』擺布了。
雖然我不同情他，但還是祈禱他能在來世過上平穩的生活。


*  *  *


離開教會之後，赤紅的太陽已經臨近地平線。
「呼」
上一次去服裝出租店租禮服，是在店建好之後的慶祝派對上。
這次，卻是同伴的葬禮。這讓我有種奇怪的感覺。
「哥哥」
「怎麼啦？」
「對不起，全都讓哥哥自己承擔」
我摸了摸露媞的頭。
「謝謝你的擔心」
我並不後悔殺死亞雷斯。雖然不後悔，但老實說我已經再也不想手刃同伴了。
「果然，我還是適合在這個佐爾坦悠閑度日啊」
壞掉的銅劍也還沒有買新的。現在我的腰上並沒有武器。在亞雷斯的葬禮結束之前，我實在是提不起心思去買新武器。
「我也終於變了啊」
在佐爾坦剛開始慢生活的時候，雖然我有在避免戰鬥，可沒有武器總是靜不下心來。
坦塔患上白眼病，岡茲來向我求助的時候，我在離開玄關時也帶著武器。
之所以帶著銅劍而不是鋼劍，也是我對仍未完全捨棄的戰鬥習慣所作出的最大限度的抵抗。
「回去吧」
露媞抱著我的胳膊嫣然一笑。她的腰上也沒有佩劍。
我也對她回以笑容，我們就像是隨處可見的兄妹一樣，漫步在佐爾坦的大街上。

在這個充滿戰鬥的世界裡，想要不靠武器活下去是很難的。明天我也打算去購置新的銅劍。
但是，我想以自己的意志為了保護重要的人而拿起武器，而不是受到『加護』的驅使。同樣也想以自身的意志去揮劍，以及使用『加護』的技能。

一邊與露媞並肩前行，我一邊思考著這些事。


*  *  *


2天後的夜晚。
「終於收拾好了啊」
「是呢」
我和莉特終於將被亞雷斯破壞的店整理完畢。

除此之外還有張羅亞雷斯的葬禮以及達南的住院手續，本來應該還得花上更多時間才是，這都歸功於除了雇來的工匠之外，還得到了露媞和提瑟，岡茲還有坦塔，以及米德跟娜歐，紐曼，斯特姆桑達，歐帕拉拉，阿爾，阿迪米，冒險者公會的朋友以及不值班的衛兵們等等各路人馬的幫忙。

之後為了慶祝店鋪修繕完畢以及感謝大家幫忙我們召開了派對，在結束時我將露媞這個妹妹介紹給了大家，大家又是驚訝又是歡迎，度過了一段很快樂的時光。
其實我們剛才說的收拾指的是宴會的善後，被亞雷斯破壞的那些部分在下午就已經搞定了。
「唉」
莉特嘆了口氣。自從看到店被破壞的不成樣子之後她就一直是這個狀態。
「要是亞雷斯還活著的話我肯定要胖揍他一頓」
莉特小聲這麼嘟囔著開始揮起了空拳。
我輕輕一笑，為莉特泡一杯茶。
「給，喝了它就去睡吧」
「嗯」
聽了我的話莉特老實地坐在椅子上喝起了茶。
她的情緒似乎終於有所緩解。
「......床」
「嗯？」
莉特看著手邊的杯子喃喃道。
「我們的床被弄壞了」
「啊......是啊」
貴族夫人很喜歡在床單被褥裡藏一些秘密文件。
亞雷斯似乎認為我出於某種目的和露媞取得了聯絡，在瘋狂尋找那個根本不存在的書信。

當然，就算與露媞有書信來往我和莉特也不會把信藏在那種地方......亞雷斯應該實在是走投無路了吧。
「那可是我們的床啊......」
莉特的視線落在手邊的茶杯上如此說道。
她的聲音有些顫抖。
「莉特......」
莉特受到的打擊要比我想像的還要大。
到底是怎麼了？
我來到了她的旁邊。
莉特靠在我的肩上，一反往常用有氣無力的聲音說道。
「我晚上睡覺的時候，有時候會非常不安」
「不安？」
「害怕明天早上起來的時候雷德會不會就不在了」
「說什麼傻話呢你」
「但是，達南他......他清楚你很優秀。果然不止是我自己，大家都需要你的力量。你是『引導者』，只要和你在一起，不管是誰肯定都能得到你指導的幫助」
「『引導者』可不是那麼了不起的東西。除了初始加護等級很高之外，它沒有任何固有技能，衝動也非常小。更沒法用魔法和武技」
「不對哦。我覺得這些並不是加護原本的意義。最重要的既不是技能也不是衝動，而是“你自己”」
莉特看向我的眼睛。
「你強大的地方就在於你是『引導者雷德』。大家所依靠的，是由與這個加護一同一路走來的人生所塑造出的雷德這個人類」
「是嗎......」

『引導者』這個加護的職責，是保護好才剛剛啟程的『勇者』。為此它的衝動是必須要保護『勇者』，但由於衝動的強弱和加護的強弱成正比，因此『引導者』的衝動非常小。

所以，我並不覺得自己有受加護的擺布。也不可能會有去引導露媞之外的人的職責。
但是，也許就如莉特所說。
我獲得了引導露媞的義務與力量。為此也付諸了努力。
而這些經驗，也幫到了露媞之外的人。
仔細想想其實這也是正常的。

但是莉特的這番話，在這個以加護為人生中心的世界中可以說是一種標新立異的想法。
「我呢......很了解這樣的雷德。因為在羅加維亞的時候，你也引導了因為冥頑不靈導致失敗而陷入絕望的我。但是，這或許讓我變得有些自負起來」
凝視著我的莉特的眼神搖曳了起來。
「所以當我聽說達南來到了這裡......一心想要打倒破壞了他故鄉的魔王報酬的他竟然不惜終止旅途跑來找你的時候，我才知道原來大家也都清楚你的力量......這讓我很開心，但也讓我很害怕。因為現在理解你的已經不止我一個了。這不就代表在你身邊的就算不是我也沒關係嗎。我不禁就這麼想」
「...........」
「我知道雷德希望和我一起生活，你也是這麼告訴我的。但是，即便如此，我還是害怕你會不會為了引導其他人而離開。在羅加維亞分別的時候我大哭了一場，總算是忍耐了下來......但是現在的我無法忍耐你不在身邊」
莉特用雙手牽起我的手，依賴似地握住了它。
「這個家，還有那張床是我和雷德的歸宿。是我們在佐爾坦一起生活的回憶。所以看到它們被破壞的不成樣子，感覺就像是失去了你可以回來的地方一樣......」
「你錯了！」
莉特這番發言讓我忍無可忍地吼了出來，語氣之強令我自己也不禁大吃一驚。
看著被嚇到愣在那裡的莉特，我後悔聲音太大了，但即便如此我也必須要把心中的感情傳達給她。
「你搞錯了莉特」
「搞，搞錯了？」
我從椅子上起身，在莉特面前跪了下來。
因為我個子更高，所以站著抱住她的話，我的視線會在她之上。
但是，跪下去之後坐在梯子上的莉特的臉自然就高出了我的視線。
「雷，雷德？」
我默默地將手環過一臉困惑的莉特的腰部。
接著將臉埋在她的胸口用力抱住了她。
「我需要你」
莉特好像在擔心如果有人需要我我是不是就會離開......她錯了。
就像是她需要我一樣，我也同樣需要她。
「不管是這個店還是那張床，對我來說它們之所以能稱之為歸宿，就是因為那裡有你在。我的歸宿是你啊，莉特。如果沒有遇到你的話，我大概就會捨棄在佐爾坦的慢生活接受達南的邀請了吧。因為沒有你在這裡自然也不再是我的歸宿」
「啊......嗚......」
「還記得在佐爾坦重逢的時候，你對我說過，我也有不知道的事以及缺根筋的部分嗎？」
「嗯，當然記得。和雷德說過的話我都記得」
「當時，你不是說過正是因為你清楚我也有缺點，所以才想和我在一起的嗎」
「嗯，是說過......」
「我很開心。因為還是第一次有人對我那麼說」
我是『引導者』。
職責是在『勇者』啟程的時候負責引導她。而就像莉特說的那樣，這股力量大概也能用來引導其他人吧。

但是，引導的對象變得越強我的能力就越是會派不上用場。我教不了武技和魔法。
我之所以會選擇放棄，並不是上了亞雷斯的當。最大的理由在於我明白自己不管如何努力總有一天也會迎來極限。
「因為我是『引導者』。所以會引導某人，然後在他成長到足夠強大之後離開」
因此。
「我們不會一直在一起。最後大家都會到我所遙不可及的地方去。這就是我的職責」
如果說『勇者』是總有一天會變得比任何人都要強，輪誰都望塵莫及的加護的話，『引導者』就是總有一天會被所有人拋棄的加護了。
「但是，就算我被趕出隊伍，不再是能夠引導你的英雄......明知我也有缺點，你卻告訴我，正因如此才想和我在一起。正是因為經歷了那趟拚命追趕的旅途，日常（莉特）才能來到我的身邊」
我用力抱緊了莉特的身體。仿彿在抓緊她不讓她到任何地方去一樣。
「......雷德」
「這就是我旅途的終點。我可是要遠比你想象中的還要喜歡你啊」
「............！！」
「所以，不管發生什麼我都不會離開你。反倒是我也害怕你會離開我。在得知席桑丹還活著的時候，我害怕的並不是慢生活被打破，而是你拒絕我要獨自去戰鬥」
能感受到莉特的心跳在加速。她用力抱住了我的頭。
「雷德肯一直和我在一起嗎？」
「這是我發自內心的心願」
我將自己所有的心意都傳達給了莉特。
「所以不要再為害怕我會離開而不安了。我再說一次，我最喜歡莉特了」
「嗯......我不會再那麼想了」
莉特的身體在顫抖。能夠聽到從上方傳來的嗚咽聲。
「對，對不起，但是，不知怎麼地，眼，眼淚，就是停不下來」
不知道我們保持這個姿勢持續了多久。
但這是一段幸福的時光。接著莉特開口道。
「我們都沒法再一個人活下去了呢，感覺我們是不是變弱了」
「可能是吧。但是慢生活裡並不需要強大的力量吧？」
「嗯」
變弱也無妨。這個想法在這個充滿了戰鬥的世界裡也許是錯誤的。
但是現在，我和莉特非常幸福。
我並不覺得，這份心情是錯誤的。
「......我想去床上」
聽到莉特小到快要消失不見的聲音，我抬起頭來，接著便看到了莉特那張通紅的臉蛋。
「嗯，嗯，我......也想去」
我的話，讓莉特的臉變得更紅。


*  *  *


更深夜靜。我和莉特正面對面坐在床上。

床上已經鋪上了斯特姆桑達為我們準備的嶄新白色床單。
我們面對面，寡言少語又坐立不安地不斷對上視線又移開。

我到底在搞什麼啊，這讓我自己也感到無語。
莉特換上了睡衣，將前面的紐扣解到了第三顆。

所以原本能看到她直到胸口部分的肌膚才是，但她現在抱著枕頭，把胸口和掛著傻笑的嘴巴都給擋了起來。
該怎麼說呢，她這事到如今才開始遮羞的動作也好可愛啊。
「莉特」
我下定決心，打算將距離拉進到膝蓋可以相互觸碰的位置。
「雷德」
莉特也在向我靠近。
我們的膝蓋相互錯開，莉特那柔軟又溫暖的大腿碰到了我的大腿。
僅是如此便讓我的臉變得滾燙無比。

我朝莉特伸出手......。
「那個，莉特，能把枕頭放下來嗎？」
「嗚——......就這麼幫我脫吧」
「誒，誒——......那不是就看不見手在哪了嗎」
「你辦得到吧！我也會這樣幫你脫的」
既然她這麼說那就沒辦法了。
我們夾著枕頭開始解起了對方睡衣上的紐扣。

一般來說這並不是件複雜的工作，不過我碰到的這個柔軟的觸感是莉特的胸部啊......要說我沒有動搖那是騙人的。
話說正因為看不見所以意識反而都集中在觸摸著莉特身體的指頭上面去了啊。

莉特的肌膚滑滑的就像是絹絲一樣。
舒服地讓我想要永遠就這麼摸下去。
我不禁用手指輕撫了下莉特的谷間。
「啊嗚」
莉特叫了一下。在我吃驚的時候枕頭從手臂滑落倒在了床上。
莉特從肩膀到胸口的肌膚全都裸露了出來。
她的身體真是太美了，美到讓我不禁為之神魂顛倒。
莉特小臉通紅，濕潤的天空色眼瞳正在不斷搖曳。
看著這副模樣的莉特，我心中對於她的愛便止不住地爆發了出來。但是在我動手之前她便搶先採取了行動。
「討厭！這是回禮！」
莉特雙目緊閉，把雙手伸進了我的睡衣裡。
她的手指在我的胸部和腹部蠕動起來。
快感瞬間直衝後背......但這只是一開始。接著立馬便。
「唔，等一下莉特，好癢啊！啊哈哈哈！！」
莉特不知是不是到達了害羞的極限，她閉著眼睛開始撓起我的側腹。
莉特那能夠準確無誤地揮動彎刀這種體積大難以使用的武器的手指將它的靈活性發揮的淋漓盡致，瘋狂在我的側腹撓起了癢癢。
「我不行了莉特！啊哈哈！」
「怎麼樣投降了嗎！」
什麼投不投降的，根本不明所以。
她本人恐怕也因為太過害羞搞得自己也不知道自己在說什麼了吧。
話說現在我們的手都在對方的睡衣裡，這個姿勢可不妙。

照這麼下去......。

啪嚓。
「啊」
在我因為癢而扭動身體的時候，把莉特睡衣的紐扣給扯下來了。
接著睡衣大敞，莉特的胸部噗呦地一下露了出來。
莉特睜大雙眼。我則是視線被釘在了她的胸部上。
「...........」
「...........」
我們兩個都僵在那裡，只聽牆上的壁鐘的時針傳來滴答滴答的聲音。
「呀！」
回過神來的莉特抱住了我。
確實抱在一起可以遮住胸部，但緊貼在我胸口的那股溫暖又柔軟的觸感卻讓我各方面的理性都快要突破臨界點了。
「看到了？」
「現在才問我看沒看到嗎......很美哦」
「討厭！」
莉特加大雙臂的力氣抱緊了我。她身上的香味飄入鼻腔，令我心中的愛在不斷泛濫。
回過神來，我也用力抱住了莉特。
「感覺光是這樣就滿足了呢」
莉特在我的懷中露出陶醉的表情如此說道。
「確實......光是這樣觸碰彼此就感覺很幸福了啊」
懷中的莉特用天空色的眼眸看向了我。
僅是如此，便讓我產生了比得到任何財寶都要幸福的感覺。
「但是」
莉特吻向了我的脖子。
「雖然這樣很幸福，卻很難受」
她在耳邊的輕語，令快感直衝脊梁。

我們放開彼此拉開了距離。
我們走下床，彼此面對面站了起來。

接著不再夾著枕頭，默默地脫起了彼此的衣服。
當最後一件衣服從莉特的身體滑落至地板之後，在射入窗戶的月光的照耀下，一絲不掛的莉特出現在了我的眼前。

(插圖010)

「好害羞呀......」
莉特抱住自己的身體用雙臂擋住了胸部。
這個舉止感覺非常嫵媚，反倒是讓我害羞了起來。
「雷德在傻笑欸」
莉特看著害羞的我如此說道，不過莉特的臉上也掛著和我同樣洋溢著感情的笑容。
「那個，呃.......嘿！」
扭扭妮妮的莉特做好覺悟，衝我撲了過來。
我抱住莉特的身體，全力發揮出雜技技能的能力，維持著互相擁抱的姿勢落在了床上。
（嗚哇啊啊啊啊啊啊啊啊）
我們同時在心裡大喊起來。
當然沒有讀心之類的技能的我是不可能知道莉特心裡在想什麼啦......但是，我確信現在這個瞬間莉特肯定和我同時在心裡喊了出來。

我們其實經常擁抱在一起。
雖然那是非常幸福的時刻，但並不至於到忘我的程度。

可是，像這樣肌膚直接互相觸碰卻又是一種未知的體驗。
莉特的肌膚光滑細膩，摸上去舒服到感覺會讓人上癮一樣。現在她稍微出了一些汗。鍛鍊的恰到好處的肌肉十分緊致又健康。
她的腿十分修長。但是大腿附近卻很有肉感，緊挨在上面的腿所傳來的感覺十分舒適。

我的胸部和莉特的胸部直接緊貼在一起，她的那份柔軟毫無保留地傳達了過來。
金色的秀髮總覺得有一股很好聞的味道。
莉特那有些急促的呼吸在瘙癢著耳朵。

她的後背到臀部描繪著一條優美的曲線。但是那條曲線上還隱約地殘留著一些古舊的傷痕。這些大概是被箭射傷的吧。

當冒險者不可能會毫髮無損。雖然能用魔法治療傷口，但也會有傷疤留下來。
用手指輕輕撫摸莉特背上的傷痕之後，她的身體抖了一下。感覺就連她身上的這些傷疤，也讓我覺得如此令人怜愛。

我可以用所有能夠觸碰到的部分直接感受到莉特的存在。
也就是說......現在充滿了幸福感。
滿滿地幸福就好像要從名為心的容器中溢出了一樣。
「雷德」
「莉特......！」
光是互相呼喚對方的名字就令內心為止顫抖。
「我喜歡你，我愛你，我喜歡到根本不知道該如何表達才好」
我已經連自己都不知道自己到底在說些什麼了。
感覺我真是完全不行啊。

莉特激動地濕潤著雙眸將臉湊向了我。
我們雙唇相疊，莉特的味道在口中擴散開來。

感覺這一吻相當之長。但可能其實並沒有那麼長。
身為騎士為了配合突擊或者奇襲的時機所鍛鍊過的時間感，感覺也不知道被拋在了哪裡。
比起那種技術，我將一切都集中在了眼前的莉特身上。
「莉特，抱歉，我已經到極限了，大概已經忍不住了」
「那種事......我肯定也一樣呀」
莉特這番痛苦難耐般的發言令我頭暈目眩。

這次輪到我吻向了她，然後一邊接吻一邊撫摸起她的身體。
雙唇相離之後，臉頰泛起紅潮的莉特露出了心花怒放的笑容。
「最喜歡你了」
她的這聲低語，終於令我的感情從名為心的容器中爆發了出來。

之後的事自然不言而喻。


*  *  *


莉特正躺在我的懷中。

躺在床上的我們都渾身無力。
我因為實在是太過滿足，感覺會有一段時間動彈不得。莉特肯定也一樣吧。

我們只是偶爾與身旁的愛人互相對視，嬉笑著將額頭貼在一起，或者輕啄一般互相親吻......。
「好開心......」
莉特將臉貼在我的胸口如是說道。
「能和你一起生活，真的讓我好幸福......感覺再也沒有比這更幸福的事了」
莉特撫摸著自己的腹部臉頰泛起了紅潮。
「現在就感覺你好像還在這裡一樣，好幸福呀」
「從今往後會更幸福的」
「是嗎？」
「當然。剛才我說這裡是旅途的終點，但其實也是新旅途的起點」
「那是什麼樣的旅途呀？」
「是一趟很平穩卻又美妙的旅途」
我閉上眼睛，想像著旅途的過程一邊繼續道。
「我們一起在店裡工作，休息的時候一起出門，在春天的花田中吃便當，在夏天的大海中暢游，在秋天的紅葉下手牽手，最後在冬日的雪景中相擁......到時候家人應該也會增加吧，可能是男孩，也可能是女孩，日子肯定會過得熱鬧又開心。然後我們又會變回2個人，到那時可能會覺得這份安寧有些寂寞。現在這嶄新的店面到時候也會完全上了年頭，給吱吱作響的門上完潤滑油之後，早上我們又像現在一樣開張營業。總有一天我們也會完全變成老爺爺老奶奶吧，說不動到時候還要照看孫子孫女。然後日子又變得熱鬧起來，雖然辛苦卻也很開心......」
「這躺旅途......真是美妙呢......」
「是啊。我們一起走到最後吧。當旅途走到盡頭之後，我們還可以將這句話再重複一遍。“真是一場美妙的旅途”」
莉特坐起身體，用天空色的眼瞳看向我。
「吶莉特」
「怎麼啦？」
一邊覺得她的眼睛真美，我一邊向她問道。
「之前就想問你了，你喜歡什麼樣的寶石？」
莉特壓在我身上抱住了我。
「只要是你選的就行！你專門為我選的寶石就是我最喜歡的！」
「明白」
「那我就拭目以待了哦！我可是說真的！」
「我可沒法像過去還在冒險時那樣搞到稀有的寶石啊」
「這跟稀不稀有沒一點關係，我也不會在意它的價值還有周圍人的評價」
壓在我身上的莉特嫣然一笑。她爽朗又開心地露出了兩排皓齒。
「那可是連接著你我的寶石。是要比任何傳說中的寶石都珍貴的寶物哦」
接著莉特又吻向了我。
持續著深情地長吻之後，內心又火熱了起來。

這令我慾火難耐，回過神來我又緊緊抱住了莉特的身體。


*  *  *


第二天。
外面下起了雨。
「戒指嗎」
我把知道的所有寶石的種類全都列在了便簽上，思考著哪個最適合莉特。

在佐爾坦寶石的需求量並不高。
中央那邊自然是上到鑽石下到參差不齊的珍珠，無論是高級品還是廉價貨都應有盡有。
但佐爾坦就不一樣了。
這裡所流通的只有旅行商人和水手們帶來的寶石，並不是隨時都能買到各種各樣的寶石。
「難道去找貴族買嗎。但是又不知道哪個貴族有哪種寶石啊」
而且預算也不多。
「這麼一來，只能上山了嗎」
世界盡頭之壁那邊，應該會有十分了解寶石的寶石巨人的村落。
不然去找之前救下的住在世界盡頭之壁山腳的怪物祖格們諮詢一下看看好了。
「時間就定在冬至祭的晚上吧」
雖然很俗套，但要交給她的話還是冬至祭的晚上比較好吧？
在此之前得準備好寶石把戒指做出來才行。
「喂!雷德，你在幹什麼呢」
前來避雨的岡茲露出猥瑣的笑容這麼說道。
我冷淡地回了句「沒幹什麼」。

再有1個小時莉特應該就要回來了。
一想到這件事我的嘴角就止不住地上揚了起來。


*  *  *


自露媞開始在佐爾坦生活之後時光飛逝，這就已經過去1周時間了。
「謝謝惠顧」
我向買了藥的顧客低頭行了一禮。
新買的銅劍正收在劍鞘裡放在櫃台下面。

雖然被鐵匠鋪的莫格里姆抱怨說「又是銅劍嗎」，但不管怎麼說我對這個廉價的武器還是情有獨鐘的。
它就類似於我要過上慢生活的決意的象徵。
「雷德，藥都送完了」
莉特與客人交替著回到了店裡。
她把空蕩蕩的藥箱收拾起來，坐在了我的旁邊。
接著在同一時刻，從店後面傳來了小跑的聲音。
「哥哥，院子裡的藥草我們都打理完畢了」
是露媞和提瑟。

從今天開始直到露媞的藥草園開張之前，她們都要在院子裡學習藥草的培育方式。
今天我讓她們負責去修剪一種名叫赤卵的藥草。

赤卵是一種能夠長成不到一米高的灌木赤卵樹的紅色果實，但在冬季它會在葉子凋落之後停止活動。
在它冬眠期間，需要把樹枝砍掉來集中養分。到了這種時期就放心大膽的砍掉其他樹枝，只留下三分之一就好。
赤卵的收獲時間是初夏，而在氣溫較高的佐爾坦收獲時間會提前的相當早。雖然這個植物是一種藥草，但它的味道像茄子，所以也會用作高級料理的食材。

它具有清熱退燒的效果，能夠用於緩和哥布林熱等較為危險的熱病的病情。
在佐爾坦這種藥草的需求量很高，但野生的果實體型很小。將它培育變大的過程相當有趣，而且還會有種成就感。

是一種十分有培育價值的藥草。
「辛苦你們了，之後我會去檢查，你們可以先休息一下」
「......我也來幫忙」
言畢，露媞在我的旁邊，也就是莉特所在的相反側坐了下來。
「沒關係嗎？你應該挺累了吧？」
「腳累的時候坐下來會非常舒服」
露媞這麼說著輕輕一笑。
接著她拉起我的左臂，用力抱住了它。
「唔！」
為了與之抗衡，莉特也抱住了我的右臂。
手臂上傳來了一股露媞所沒有的波濤洶涌的觸感。
「唔唔唔」
露媞很不甘心似地眯著眼瞪向了莉特。
莉特也不服輸地帶著從容的表情瞪了回去。店裡一瞬間充滿了劍拔弩張的氣息。但是，
「噗，啊哈哈哈哈！」
她們2人又忍俊不禁，放聲開懷大笑了起來。
「你們在幹什麼呢」
我一邊苦笑，一邊感覺著從露媞身上散發出的那股祥和的氣氛眯起了眼睛。
在那裡的並不是莉特表示“可怕”的『勇者』。而是一個名為露媞的普通少女。
「這種情況，我該怎麼搭話比較好啊」
提瑟看著我們，一副半無語，半對看到露媞幸福的樣子而感到開心的模樣這麼說道。
提瑟肩膀上的嚇嚇先生也敲了敲她的肩膀，似乎在為露媞的變化感到開心。

叮鈴！

店門口的門鈴猛地響了起來。
「露，露露小姐！」
跑進來的事冒險者公會的職員梅古莉雅小姐。
「我們接到了緊急委託！從世界盡頭之壁下來的食人魔集團佔領了村莊......前去處理的C級冒險者也被它們抓起來了！」
露媞放開我的胳膊站了起來。
「我知道了」
看到露媞點點頭，我將放在銅劍旁邊的她的劍交給了她。那是上面打著洞的哥布林劍。
「哥哥，我去去就回」
「啊，注意安全」
佐爾坦唯一的B級冒險者畢伊現在下落不明。而露媞和提瑟的小隊填補了這個空缺。

不過這說到底也只是副業。
附加上只會在藥草園有空閑的時候幫忙的條件，在佐爾坦當局前來打探詢問時露媞答應了他們提出將她升級為B級冒險者的建議。

她作為冒險者的名字，是“露媞·露露”。
她和我不同似乎不習慣平時就使用假名，所以決定讓外人叫她露露，而親近的人則叫她露媞。

露媞只在普通衣服上面套了一件鑲有鐵片的胸甲。
雖然只穿著這種防具去戰鬥說不上是準備充分，但露媞似乎還是決定以這種不是太偏向於戰鬥的打扮風格去戰鬥。
露媞已經不再是『勇者』。也不再有會被強行要求去幫助他人的衝動。

但是，這並不代表露媞就變成了會對有難的人見死不救的人。
起初，她似乎對於幫助他人這件事還有所猶豫。

但是，
「好不容易才獲得自由。按照自己的想法來就好，不要被勇者這個身份束縛」
當我這麼告訴她之後，她便如釋重負，決定成為一個想幫就幫，不想幫就不幫的冒險者。
「果然露媞大人是勇者呢。她現在是一名以自身的意志選擇戰鬥的勇者，而不是出於加護所賦予的責任義務」
「是啊」
我點頭同意著提瑟的說法。

這便是露媞的慢生活。不受『勇者』束縛，過上屬於勇者的慢生活。
那英譯颯爽邁步向前的背影，已經不會再受任何因素的影響。
「露媞！等你回來之後給你做你愛吃的。想吃什麼？」
我出聲如此詢問之後，以自身的意志踏上屬於自己的人生的少女驀然回首。
「我想喝蜂蜜牛奶」
言畢，我的妹妹露出了十分自然，並且又無比可愛的笑容。

『勇者』獲得了拯救。

可喜可賀。

但是露媞的故事並沒有到此結束。
因為今後要度過的日常，才是她的心之所望。
她的慢生活接下來才要剛剛開始。

(插圖011)
