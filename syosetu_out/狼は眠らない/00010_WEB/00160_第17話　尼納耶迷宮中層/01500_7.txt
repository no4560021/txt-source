既然沒有聽覺，那麼依賴視覺來行動的可能性就很高。

抱著這想法驗證之後，果然便確認到，侵入者的部分身體、腳尖等進入視界的瞬間，就會開始降下。
在看清這幾階層的魔獸的特質一事上，雷肯判斷已經足夠了。
之後就快很多了。
雷肯一邊不斷前進，一邊對正上方的魔獸放出〈炎槍〉。
然後以〈移動〉從屍體拔出魔石。
掉下的魔石，由艾達、阿利歐斯、赫蕾絲照順序接下。
以這做法馬不停蹄地前進。

第二十二階層。
第二十三階層。
第二十四階層。

然後第二十五階層，也接近出口了。

「停下。在這裡休息。坐下也沒關係。」
「不。為什麼要在這種危險的地點休息。」
「剛剛打倒的是大型個體。等它再湧出一次來打倒，製作〈印〉。」
「是這樣阿。嘿咻。真是的。這可是被稱為〈惡夢的中層〉阿。對雷肯殿來說，就宛如無人的荒野呢。」
「這區域完全沒冒険者呢。」
「因為中層沒有甜頭阿。大家都會盡可能早點通過這裡，到第三十一階層以下狩獵。所以，狩獵第三十一階層的大型個體的委託很有人氣。」
「妳說委託？」
「魔獸每下一階層就會變強。對總算踏破了第三十階層的隊伍來說，第三十一階層的敵人太難纏了。更別說連續打倒兩隻大型個體，簡直難上加難。」
「啊啊，原來如此。」
「所以在冒険者協會，製作第三十一階層的〈印〉的委託會比較貴。費用是大金幣三枚。」
「大金幣三枚！阿～阿，不久前，我明明也是擁有大金幣五枚的有錢人。」

為了買弓和箭矢，以及準備防具，前一次的探索得到的金幣五十枚，也就是大金幣五枚中，還在艾達身上的應該只剩下遠低於金幣十枚的量。

「艾達殿。如果是這支隊伍，應該很快就能再得到好幾枚大金幣。打起精神吧。」
「嗯。謝謝，赫蕾絲小姐。」
「艾達殿的心總是很有精神呢。那是難有的資質。剛剛說到什麼了。啊啊，製作〈印〉的委託對吧。委託方有個條件是，要持有第三十階層的〈印〉。」

從上層轉移到別階層的場合，會出現在該階層的入口側。相反的，從下層轉移的話，會出現在目標階層的出口側。也就是說，持有第三十階層的〈印〉的隊伍，轉移後要往第三十一階層前進的話，首先有必要通過第三十階層。

大型個體不怎麼會出現在入口側，而根據這迷宮的第三十一階層的地圖，大型個體的湧現場所也在出口附近。

也就是說，接下這委託的話，有必要踏破第三十階層和第三十一階層，然後連續打倒大型個體兩隻，但這點工作就能有大金幣三枚還是很賺。就算是六人隊伍，一人也能在半天的工作中得到金幣五枚，那麼對接受方來說，沒什麼好困擾的吧。

「當然，委託途中得到的魔石、素材和寶箱，都屬於接受委託的隊伍。至於依賴方，在素材剝取和搬運行李上幫忙也是條件之一。第三十一階層的素材能高價賣出，所以只要慢慢地邊狩獵邊前進，就能得到跟委託報酬同等程度的收入。」

那麼一天就能賺到大金幣一枚。毫無疑問是很賺的工作。

「嘛，畢竟要花上兩天，而且詛咒和毒也是由接受委託的一方來負擔。裝備也多少會有損耗。但依舊是利潤很高的委託沒有錯。」
「兩天？」
「有疑問的是這裡？以雷肯殿的基準來考量的話可困擾了。」
「〈炎槍〉！」

雷肯突然舉起右手，對上方放出攻擊魔法。
是大型個體再次出現了。

「〈移動〉。」

右手接住落下的魔石，收到〈收納〉裡。
雷肯無言地走起，三人跟在後頭。

在走了一會抵達的場所，雷肯停了下來，坐下。

「在這裡吃飯吧。」
「這種場所沒問題嗎？」
「這上面跟周遭都沒有蜘蛛。要是湧現了就會說。」
「這樣阿。」

雷肯取出了薪柴、鍋子、水和食材。
艾達以魔法點火，流利地製作湯。
一行人放下心來，品嘗著溫暖的湯。

「在迷宮能吃到這麼溫暖的食物，真的很奢侈呢。」
「溫暖的食物能調養身體，給予精力。戰力也會加倍。我在迷宮裡，也會盡量吃溫暖的食物。」
「不。其他的迷宮說不定能做到那種事，但這迷宮可收集不到薪柴。雷肯殿的〈箱〉究竟是怎麼回事，實在很讓人覺得不可思議。難道說，是鴉庫魯班多・托馬托卿特製的嗎？」
「鴉庫魯班多？那名字好像有聽過，是誰？」
「明明已經決定不再為雷肯殿的話感到驚訝了，但還是感到驚訝了。連鴉庫魯班多殿和托馬托商會都不知道，是怎麼在這国家生活的？」
「我是從遠方來的。進入這国家時是在鄉下。最近才開始在城鎮居住。」
「應該有看過夜光燈、退魔燈和魔力筆吧。沒聽過昏迷棒、長飛弓、破裂矢或貫通槍嗎？那些全都是由鴉庫魯班多殿開發，並從托馬托商會銷售的。」
「昏迷棒有看過。這麼說來，夜光燈和退魔燈好像也有看過。」
「是吧。不可能沒看過的。要再說的話，〈真實之鐘〉、〈雙子之鏡〉、〈天界之翼〉、〈清淨之網〉、〈樂園之聖燈〉、〈審判之天秤〉等等，對国家來說不能沒有的種種重要的魔道具，也都是鴉庫魯班多殿製作的。」
「赫蕾絲小姐。」
「怎麼了嗎。」
「〈真實之鐘〉是神殿的聖具，不是能隨意說出口的東西。此外，〈雙子之鏡〉、〈天界之翼〉、〈清淨之網〉是王宮隱匿的軍事機密，只有高位貴族知道那是什麼。〈樂園之聖燈〉是由王国騎士團，而〈審判之天秤〉是由王国魔法士團獨佔的，其情報沒有被公開。〈鴉庫魯班多之昏迷棒〉有規定只有王都警備隊和王国騎士團能持有。」
「阿。」
「另外，貴族和大商人才會熟悉於托馬托商會的製品。跟庶民沒有緣分。雷肯先生，艾達小姐。請把剛剛的話忘記。」
「知道了。」
「嗯！我很擅長忘記喔。」
「然後，為了不產生誤解所以說一下，鴉庫魯班多這名字是代代相傳的，所以魔道具並不是都由現在的鴉庫魯班多殿開發的。」
「是那樣嗎？」
「赫蕾絲小姐。您認為〈雙子之鏡〉是從何時存在的？那在這国家建国後不久就存在了喔。要是沒有那個的話，這国家的南半部分會變成多雷絲塔王国的也說不定。」
「之前都不知道。阿利歐斯殿，那些知識是從哪裡得到的？」
「是流派的秘密。」
「不，這是騙人的吧。」